        <div class="welcome">
            <img src="./img/logo.png" width="533" height="267" />
        </div>
        <div class="loginContainer">
            <div class="mTitle">Login</div><br/>
            <div class="sTitle">Non hai ancora un account? <a href="?p=register">Registrati qui</a></div><br/>
            <div class="login_form">
                <form method="post" id="logform">
                    <input type="text" name="email" placeholder="Email" class="roundInput"><br>
                    <input type="password" name="password" placeholder="Password" class="roundInput"><br>
                    <div style="display: inline-block; width: 100%;">
                        <div style="display: inline-block;position: relative; left: 5px;">
                            <input type="checkbox" name="remember" value="1" /> Ricordami
                        </div>
                        <div style="display: inline-block;position: relative;float: right;">
                            <a href="#">Hai dimenticato la password?</a><br>
                        </div>
                    </div><br/>
                    <div style="width: 100%; text-align: right;">
                        <div class="login_button" onclick="document.getElementById('logform').submit();">LOGIN</div>
                    </div><br/>
                    <div class="fb">
                        <span>O registrati con</span><br>
                        <a href="#"><img src="./img/fb.png" border=0 height="39" width="89"></a></a>
                    </div><br/>
                    $$error$$
                </form>
            </div>
        </div>