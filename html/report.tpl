<div class="newReport">
    <form method="post" id="mForm" enctype="multipart/form-data">
        <div class="sTitle">Tipologia segnalazione</div><br/>
        <div class="cat_container">
            <div class="categories img1" data-id="1"></div>
            <div class="categories img2" data-id="2"></div>
            <div class="categories img3" data-id="3"></div>
            <div class="categories img4" data-id="4"></div>
            <div class="categories img5" data-id="5"></div>
            <div class="categories img6" data-id="6"></div>
        </div><br/><br/>
        <div class="sTitle">Riscontri</div><br/>
        <div class="feedbacks"><div class="bother"></div> $$bother$$ &nbsp; <div class="danger"></div> $$danger$$</div><br/><br/>
        $$error$$<br/>
        <div class="sTitle">Dove</div>
        <input type="text" name="where" class="roundInput" style="width: calc(100% - 40px);" readonly value="$$locationstr$$" /><br/>
        <div class="sTitle">Titolo</div>
        <input type="text" name="title" class="roundInput" style="width: calc(100% - 40px);" readonly value="$$title$$" /><br/>
        <div class="sTitle">Descrizione</div><br/>
        <textarea class="roundArea" name="description" readonly>$$description$$</textarea><br/>
        $$photo$$<br/>
    </form>
</div>
<script type="text/javascript">
    var reportId = "$$rid$$";
    var selCat = "$$departmentId$$";
</script>