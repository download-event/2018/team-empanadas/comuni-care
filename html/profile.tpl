
<div class="newReport">
		<div class="sTitle"><img class="title" src="./img/User.png" border=0 width="42" height="42" /> &nbsp; $$name$$ $$surname$$</div><br/>
		<br/>
		<div class="sTitle">Reputazione utente $$points$$ - Livello: $$level$$</div><br/>
		<div class="reputazione">
			<div class="progress" style="width:$$percentage$$%;"></div>
		</div><br/><br/>
		<div class="sTitle">Badge ottenuti:</div><br/>
		<div class="badges">
			$$badge$$
		</div>
		<br/>
</div>