<div class="welcome">
    <div class="mTitle" style="color:white;">Benvenuto</div>
    <div class="sTitle" style="color:white;">Sul portale delle segnalazioni</div>
</div>
<div class="loginContainer" style="top: calc(50% - 200px) !important;">
    <div class="mTitle">Registrazione</div><br/>
    <div class="login_form">
        <form method="post" id="regform">
            <input type="text" name="email" placeholder="Email" class="roundInput"><br>
            <input type="password" name="password" placeholder="Password" class="roundInput"><br>
            <input type="text" name="name" placeholder="*Name" class="roundInput"><br>
            <input type="text" name="surname" placeholder="*Surname" class="roundInput"><br>
            <input type="text" name="birthDate" placeholder="*Data di nascita" class="roundInput"><br>
            <input type="text" name="phoneNumber" placeholder="Numero telefonico" class="roundInput"><br><hr>
            <div class="g-recaptcha" data-sitekey="6LfSQW8UAAAAAKlfyrzEjjio9zuSTg0bf7P6UOb3"></div><br/>
            <div style="width: 100%; text-align: right;">
                <div class="login_button" onclick="document.getElementById('regform').submit();">SIGN UP</div>
            </div><br/>
            $$error$$
        </form>
    </div>
</div>