CREATE TABLE Users(
  id INT(11) AUTO_INCREMENT,
  email VARCHAR(128) NOT NULL,
  password CHAR(44) NOT NULL,
  name VARCHAR(48) NOT NULL,
  surname VARCHAR(48) NOT NULL,
  phoneNumber VARCHAR(18) DEFAULT NULL,
  birthDate INT(11) NOT NULL,
  userType TINYINT(1) DEFAULT 1,
  reputation INT(11) DEFAULT 0,
  UNIQUE(email),
  PRIMARY KEY(id)
);
CREATE TABLE Departments(
  id INT(11) AUTO_INCREMENT,
  name VARCHAR(32) NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE Permissions (
  id INT(11) AUTO_INCREMENT,
  userId INT(11),
  departmentId INT(11),
  FOREIGN KEY (userId) REFERENCES Users(id),
  FOREIGN KEY (departmentId) REFERENCES Departments(id),
  PRIMARY KEY (id)
);
CREATE TABLE Reports(
  id INT(11) AUTO_INCREMENT,
  userId INT(11),
  departmentId INT(11),
  creationDate INT(11) NOT NULL,
  acceptedDate INT(11) DEFAULT 0,
  startWorkingDate INT(11) DEFAULT 0,
  locationX DOUBLE DEFAULT NULL,
  locationY DOUBLE DEFAULT NULL,
  radius DOUBLE DEFAULT NULL,
  locationStr VARCHAR(64) DEFAULT NULL,
  title VARCHAR(128) NOT NULL,
  description VARCHAR(512) NOT NULL,
  photo VARCHAR(24) DEFAULT NULL,
  status TINYINT(1) DEFAULT 1,
  previousReportId INT(11) DEFAULT NULL,
  answerDate INT(11) DEFAULT 0,
  answerText VARCHAR(256) DEFAULT NULL,
  answerAttachment VARCHAR(24) DEFAULT NULL,
  answerUserId INT(11) DEFAULT NULL,
  FOREIGN KEY (userId) REFERENCES Users(id),
  FOREIGN KEY (departmentId) REFERENCES Departments(id),
  FOREIGN KEY (previousReportId) REFERENCES Reports(id),
  FOREIGN KEY (answerUserId) REFERENCES Users(id),
  PRIMARY KEY(id)
);
CREATE TABLE Badges (
  id INT(11) AUTO_INCREMENT,
  name VARCHAR(24),
  type TINYINT(1) DEFAULT 1,
  PRIMARY KEY (id)
);
CREATE TABLE UserBadges (
  id INT(11) AUTO_INCREMENT,
  userId INT(11),
  badgeId INT(11),
  FOREIGN KEY (userId) REFERENCES Users(id),
  FOREIGN KEY (badgeId) REFERENCES Badges(id),
  PRIMARY KEY(id)
);
CREATE TABLE Feedbacks(
  id INT(11) AUTO_INCREMENT,
  reportId INT(11),
  userId INT(11),
  feedbackType TINYINT(3) DEFAULT 0, -- 0 = user subscription mark
  FOREIGN KEY (reportId) REFERENCES Reports(id),
  FOREIGN KEY (userId) REFERENCES Users(id),
  PRIMARY KEY (id)
);

CREATE TABLE DepartmentChange (
  id INT(11) AUTO_INCREMENT,
  reportId INT(11),
  fromDepartmentId INT(11),
  toDepartmentId INT(11),
  changeDate INT(11) NOT NULL,
  FOREIGN KEY (reportId) REFERENCES Reports(id),
  FOREIGN KEY (fromDepartmentId) REFERENCES Departments(id),
  FOREIGN KEY (toDepartmentId) REFERENCES Departments(id),
  PRIMARY KEY (id)
);

INSERT INTO Departments (name) VALUES ('Ambiente');
INSERT INTO Departments (name) VALUES ('Giovani, Scuola, Infanzia');
INSERT INTO Departments (name) VALUES ('Lavori Pubblici');
INSERT INTO Departments (name) VALUES ('Occupazione suolo pubblico');
INSERT INTO Departments (name) VALUES ('Sicurezza e Polizia Locale');
INSERT INTO Departments (name) VALUES ('Mobilità e Trasporti');