<?php
class xPage {
    /** @var array $css */
    private $css;
    /** @var array $js */
    private $js;

    /** @var xTemplate $header */
    public $header;
    /** @var xTemplate $footer */
    public $footer;
    /** @var xTemplate $body */
    public $body;

    public function __construct($customPage = false) {
        $this->reset();
        if (!$customPage) {
            $this->header->load('header');
            $this->footer->load('footer');
        }
    }
    public static function AvailablePages() {
        $p = glob(ROOTDIR . "html/*");
        foreach ($p as &$f) {
            $f = substr($f, strrpos($f, '/') + 1);
            $f = substr($f, 0, -4);
        }
        return $p;
    }
    public function __destruct() {
        $this->reset();
        unset($this->header, $this->footer, $this->body);
    }
    public function addJS($src) {
        array_push($this->js, $src);
        $this->pushToHeadTag('<script type="text/javascript" src="'.$src.'"></script>');
    }
    public function addCSS($src) {
        array_push($this->css, $src);
        $this->pushToHeadTag('<link type="text/css" rel="stylesheet" media="all" href="'.$src.'" />');
    }
    public function loadBody($pageName) {
        return $this->body->load($pageName);
    }
    public function setGlobalVars($varsDictionary) {
        $this->header->setVariables($varsDictionary);
        $this->body->setVariables($varsDictionary);
        $this->footer->setVariables($varsDictionary);
    }
    public function setTitle($title) {
        $this->header->setVariables(array(
            'title' => $title . " - " . WATERMARK
        ));
    }
    public function reset() {
        $this->css = array();
        $this->js = array();
        if (is_null($this->header))
            $this->header = new xTemplate();
        $this->header->reset();
        if (is_null($this->body))
            $this->body = new xTemplate();
        $this->body->reset();
        if (is_null($this->footer))
            $this->footer = new xTemplate();
        $this->footer->reset();
    }
    public function pushToHeadTag($str) {
        $this->pushToTag($this->header,'</head>', $str);
    }
    public function pushToBodyTag($str) {
        $this->pushToTag($this->footer,'</body>', $str);
    }
    public function getOutput() {
        return $this->header->html . $this->body->html . $this->footer->html;
    }
    public function panic() {
        $this->loadBody("error");
        echo $this->getOutput();
        $this->reset();
        exit;
    }
    public static function ToHtmlTable($data) {
        if (count($data) === 0)
            return '<div class="row"><div class="col">Nessun risultato trovato</div></div>';

        $out = '<div class="row">';
        foreach ($data as $row) {
            $photo = (empty($row['photo']) ? 'blank.png' : $row['photo']);
            $out .= '<div class="col-md-4">
            <div class="reportBox" data-id="'. $row['id'] .'">
                <div class="reportImg" style="background-image: url(uploads/'. $photo.');">&nbsp;</div><br/>
                ' . $row['title'] . '
            </div>
		</div>';
        }
        return $out . '</div>';
    }
    /**
     * @param xTemplate $part
     * @param string $tag
     * @param string $str
     */
    private function pushToTag($part, $tag, $str) {
        $headEndPos = stripos($part->html, $tag);
        if ($headEndPos !== false) {
            $s = substr($part->html, 0, $headEndPos) . $str . "\n";
            $part->html = $s . substr($part->html, $headEndPos);
        }
    }
}
class xTemplate {
    /** @var string $html */
    public $html = '';
    /** @var string $_html */
    private $_html = '';
    /** @var string $var */
    public $varSymbol = '$$';

    public function __destruct() {
        $this->html = '';
        $this->_html = '';
    }
    public function reset() {
        $this->html = $this->_html;
    }
    public function load($name) {
        $path = ROOTDIR . "html/" . $name . '.tpl';
        if (!hasChars($name, array('/', "\x00", "\\")) && file_exists($path)) {
            $size = filesize($path);
            if ($size === 0)
                return '';
            $c = 0;
            do {
                $h = fopen($path, 'r');
                if ($h !== false) {
                    flock($h, LOCK_SH);
                    $this->_html = fread($h, $size);
                    flock($h, LOCK_UN);
                    fclose($h);
                    if (strlen($this->_html) > 0) {
                        $this->html = $this->_html;
                        return true;
                    }
                } else
                    usleep(50000);
            } while ($c++ < 3 && $h === false);
        }
        return false;
    }
    public function setVariables($varsDictionary) {
        $_ = strtolower($this->varSymbol);
        foreach ($varsDictionary as $key => $value)
            $this->html = str_ireplace($_ . strtolower($key) . $_, $value, $this->html);
    }
}