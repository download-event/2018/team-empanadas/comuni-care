<?php
define("SESSION_KEY", "\xef\x7b\x56\xd1\x91\x94\x40\x45\x2a\xc7\xbb\xbe\x19\x04\x0e\x99\xf6\x99\xad\xb4\x50\xd7\x8b\x9a\xc9\xfb\x89\x94\xa4\xcf\xdc\x61");
define("HMAC_KEY", "\xf3\xca\x20\x49\x50\xed\x46\xd3\x56\xad\x45\xb0\x00\x7d\x33\x23\x52\x27\x00\x6a\x87\xb6\x48\x38\xfe\x54\x4c\x24\x09\xed\x60\xb6");
define("TOKEN_KEY", "\x69\x52\x70\xd6\xfe\xa8\x8a\x5a\x6d\x26\xf1\xe8\xa2\xc5\x4c\xdf\xa1\xd2\x0d\x3b\xc7\x46\xf0\x6f\xa6\xdb\x37\xb4\xd6\xdb\x44\xb9");
define("DB_SERVER_HMAC", "\x00\x01\x0A\x0B\x00\x05\x0A\x0B\x00\x01\x0A\x0B\x00\x01\x0A\x0B\x00\x01\x0A\x0B\x00\x01\x0A\x0B\x00\x01\x0A\x0B\x00\x01\x0A\x0B");

function openssl_enc($encrypt = true, $data, $key, $iv = null) {
    if (is_null($iv))
        $iv = '';
    $algo = 'aes-256-' . (empty($iv) ? 'ecb' : 'cbc');
    $f = ($encrypt ? 'openssl_encrypt' : 'openssl_decrypt');
    return $f($data, $algo, $key, OPENSSL_RAW_DATA, $iv);
}
function mcrypt_enc($encrypt = true, $data, $key, $iv = null) {
    $algo = (is_null($iv) ? MCRYPT_MODE_ECB : MCRYPT_MODE_CBC);
    $f = ($encrypt ? 'mcrypt_encrypt' : 'mcrypt_decrypt');
    return $f(MCRYPT_RIJNDAEL_128, $key, $data, $algo, $iv);
}
function getCryptoLib() {
    if (function_exists('openssl_encrypt')) {
        return 'openssl_enc';
    } elseif (function_exists('mcrypt_encrypt')) {
        return 'mcrypt_enc';
    } else
        die('No crypto lib found');
}
function aes_encrypt($encrypt = true, $data, $key, $iv = null) {
    $f = getCryptoLib();
    return $f($encrypt, $data, $key, $iv);
}
function secure_random($length) { //generate cryptographycally strong random bytes
    if (function_exists('random_bytes')) {
        try {
            return random_bytes($length);
        } catch (Exception $e) {}
    }
    if (function_exists('openssl_random_pseudo_bytes')) {
        $isStrong = false;
        $b = openssl_random_pseudo_bytes($length, $isStrong);
        if ($isStrong)
            return $b;
    }
    $urand = '/dev/urandom';
    if (file_exists($urand) && is_readable($urand)) {
        $h = fopen($urand, 'r');
        if ($h !== false) {
            $b = fread($h, $length);
            fclose($h);
            if (strlen($b) === $length)
                return $b;
        }
    }
    $mcrypt_rand = 'mcrypt_create_iv';
    if (function_exists($mcrypt_rand) && defined("MCRYPT_DEV_URANDOM"))
        return $mcrypt_rand($length, MCRYPT_DEV_URANDOM);

    die('No secure random source found');
}
function packStr($str) {
    $len = pack('n', strlen($str));
    return $len . $str;
}
function peekStr(&$bin) {
    $len = unpack('n', substr($bin, 0, 2));
    $len = $len[1];
    if (strlen($bin) + 2 < $len)
        return '';
    $str = substr($bin, 2, $len);
    $bin = substr($bin, 2 + $len);
    return $str;
}
function createToken($email, $password, $username) {
    $t = pack('N', time());
    $data = $t . packStr($email) . packStr($password) . packStr($username);
    $data = hash_hmac('sha1', $data, HMAC_KEY, true) . $data;
    $iv = secure_random(16);
    $data = $iv . aes_encrypt(true,$data, TOKEN_KEY, $iv);
    return $data;
}
function checkToken($token) {
    $iv = substr($token, 0, 16);
    $token = aes_encrypt(false, substr($token, 16),TOKEN_KEY, $iv);
    $hmac = substr($token, 0, 20);
    $token = rtrim(substr($token, 20), "\x00");
    if (hash_hmac('sha1', $token, HMAC_KEY, true) != $hmac)
        return false;
    $t = unpack('N', substr($token, 0, 4));
    $t = $t[1];
    if (time() - $t > 259200)
        return false;
    $token = substr($token, 4);
    return array(
        'email' => peekStr($token),
        'password' => peekStr($token),
        'username' => peekStr($token)
    );
}
function createSession($userid) {
    $uid = pack('N', $userid);
    $t = pack('N', time());
    $data = $uid . $t;
    $data = hash_hmac('sha1', $data, HMAC_KEY, true) . $data;
    $iv = secure_random(16);
    $data = $iv . aes_encrypt(true, $data, SESSION_KEY, $iv);
    return $data;
}
function loadSession($session) {
    if (!isValidBase64($session))
        return false;
    $session = decode($session);
    $iv = substr($session, 0, 16);
    $data = aes_encrypt(false, substr($session, 16), SESSION_KEY, $iv);
    if (strlen($data) !== 28)
        return false;
    $hmac = substr($data, 0, 20);
    $uid = substr($data, 20, 4);
    $time = substr($data, 24, 4);
    if ($hmac == hash_hmac('sha1', $uid . $time, HMAC_KEY, true)) {
        $userid = unpack('N', $uid);
        $time = unpack('N', $time);
        if (time() - $time[1] > 86400)
            return false; //session expire
        return (is_int($userid[1]) ? $userid[1] : false);
    }
    return false;
}
function storeSession($session) {
    setcookie('SESSID', $session, 0, '/');
}