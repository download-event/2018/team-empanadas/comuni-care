<?php
define("ROOTDIR", dirname(__DIR__) . "/");
define("INCDIR", __DIR__ . "/");
define("REMOTE_URL", 'http://127.0.0.1/');
define("WATERMARK", "Comuni-Care");

require_once INCDIR . "config.php";
require_once INCDIR . "cryptography.php";
require_once INCDIR . "database.php";
require_once INCDIR . "utils.php";
require_once INCDIR . "template.php";

class API {
    /** @var Database $db */
    private $db;
    /** @var array $CurrentUser */
    public $CurrentUser;
    public function __construct($db) {
        $this->db = $db;
    }
    private function LoadCurrentUser($id) {
        if (!is_int($id))
            return false;
        $q = $this->GetUsers(array('id' => $id));
        $f = (is_array($q) && count($q) === 1);
        if ($f) {
            $this->CurrentUser = $q[0];
            $this->CurrentUser['id'] = (int)$this->CurrentUser['id'];
            if ($this->CurrentUser['userType'] === UserTypes::Functionary)
                $this->CurrentUser['myDepartment'] = $this->db->query("SELECT * FROM Permissions WHERE userId=".$id." LIMIT 1;", true, true);
        }
        return $f;
    }
    public function LoadSession() {
        if (!isset($_COOKIE['SESSID']))
            return false;
        $userId = loadSession($_COOKIE['SESSID']);
        return $this->LoadCurrentUser($userId);
    }
    public function Login($email, $password) {
        $q = $this->db->listTable("Users", array('email' => $email), array(), true);
        if (is_array($q)) {
            $hash = hash_hmac('sha256', $email, $password, true);
            if (encode($hash) === $q['password']) {
                $sessid = createSession($q['id']);
                storeSession(encode($sessid));
                return true;
            }
        }
        return false;
    }
    public function Register($data, $type = UserTypes::User) {
        $allow = array('email', 'password', 'name', 'surname', 'phoneNumber', 'birthDate');
        $build = array();
        foreach ($data as $key => $value)
            if (in_array($key, $allow, true)) {
                if ($key === 'birthDate') {
                    $value = strtotime($value);
                    if ($value === false)
                        return false; //invalid date
                    $value = (int)date("Ymd", $value);
                }
                $build[$key] = $value;
            }
        if (count($build) !== count($allow))
            return false; //Bad parameter count
        $build['password'] = encode(hash_hmac('sha256', $build['email'], $build['password'], true));
        if (is_string($build['birthDate']))
            $build['birthDate'] = (int)($build['birthDate']);
        $build['userType'] = $type;
        return $this->db->multiInsert("Users", array($build), false);
    }
    public function GetAnswerUser($reportId) {
        $joins = array('Reports.answerUserId' => 'Users.id');
        return $this->db->listJoin($joins, array('Reports.id' => $reportId), true);
    }
    public function ListReports($where=array()) {
        return $this->db->listTable("Reports", $where);
    }
    public function ListNotRejectedReports() {
        $q = "SELECT * FROM Reports WHERE status != " . ReportStatus::Rejected . ";";
        return $this->db->query($q, true);
    }
    public function GetUserProfile($where) {
        $out = array('user' => array(), 'reports' => array(), 'feedbacks' => array(), 'badges' => array());
        $out['user'] = $this->db->listTable("Users", $where, array(), true);
        if (!is_array($out['user']))
            return false;
        $uid = $out['user']['id'];
        $reports = $this->db->listTable("Reports", array('userId' => $uid));
        if (is_array($reports))
            $out['reports'] = $reports;
        $join = array(
            'Users.id' => 'Reports.id',
            'Reports.id' => 'Feedbacks.id'
        );
        $feedbacks = $this->db->listJoin($join, array('Users.id' => $uid));
        if (is_array($feedbacks))
            $out['feedbacks'] = $feedbacks;
        $join = array(
            'Users.id' => 'UserBadges.userId',
            'UserBadges.badgeId' => 'Badges.id'
        );
        $badges = $this->db->listJoin($join, array('Users.id' => $uid));
        if (is_array($badges))
            $out['badges'] = $badges;
        return $out;
    }
    public function GetReport($id) {
        $q = $this->ListReports(array('id' => $id));
        if (is_array($q) && count($q) === 1) {
            $q = $q[0];
            if (!empty($q['answerUserId'])) {
                $getAnswer = $this->db->listTable("Users", array('id' => $q['answerUserId']), array('id','name','surname'), true);
                if (is_array($getAnswer))
                    $q['user'] = json_encode($getAnswer);
            }
            $feedbacks = $this->db->listTable("Feedbacks", array('reportId' => $id), array('id','feedbackType'));
            if (is_array($feedbacks))
                $q['feedbacks'] = json_encode($feedbacks);
            return $q;
        }
        return false;
    }
    public function GetUsers($where) {
        $q = $this->db->listTable("Users", $where);
        return $q;
    }
    public function ListOpenReports($departmentId) {
        return $this->ListReports(array('departmentId' => $departmentId));
    }
    public function ListFeedbacks($where) {
        return $this->db->listTable("Feedbacks", $where);
    }
    public function CreateReport($report) {
        $q = $this->db->multiInsert("Reports", [$report], false);
        if ($q) {
            $reportId = $this->db->lastInsertId();
            return $this->CreateFeedback($reportId, 0);
        }
        return false;
    }
    public function CreateFeedback($reportId, $type) {
        return $this->db->multiInsert("Feedbacks", [[
            'reportId' => $reportId,
            'userId' => $this->CurrentUser['id'],
            'feedbackType' => $type
        ]], false);
    }
    public function FindDuplicates($title) {
        $words = explode(' ', $title);
        $searchFor = array();
        foreach ($words as $word)
            if (strlen($word) > 5)
                array_push($searchFor, $word);
        if (count($searchFor) > 0) {
            $q = "SELECT id, title FROM Reports WHERE ";
            foreach ($searchFor as $sWord)
                $q .= "title LIKE '%" . $this->db->real_escape($sWord) . "%' OR ";
            $q = substr($q, 0, -4) . " LIMIT 1;";
            return $this->db->query($q, true, true);
        }
        return false;
    }
    public function UpdateReportStatus($id, $newStatus, $text) {
        if (!(is_array($this->CurrentUser) && $this->CurrentUser['userType'] !== UserTypes::Functionary))
            return -6;
        if (!(is_int($newStatus) && $newStatus > 0 && $newStatus > 255 && is_numeric($id)))
            return -3;
        $report = $this->GetReport((int)$id);
        if (is_array($report)) {
            $join = array('Reports.departmentId' => 'Permissions.departmentId');
            $perms = $this->db->listJoin($join, array('Permissions.userId' => $this->CurrentUser['id']), true);
            if (count($perms) !== 1)
                return -2;
            $answer = '';
            if ($newStatus >= ReportStatus::InProgress) {
                $time = time();
                $text = $this->db->escape($text);
                $answer = ', answerDate=' . $time .", answerText='$text', answerUserId=" . $this->CurrentUser['id'];
            }
            $this->db->transaction();
            $q = "UPDATE Reports SET status=$newStatus $answer WHERE id=$id LIMIT 1;";
            $q = $this->db->query($q);
            if ($q)
                $this->db->commit();
            else
                $this->db->rollback();
            if ($q && $newStatus >= ReportStatus::InProgress) {
                $claimToken = $this->db->encodeID(TOKEN_KEY, $id, DBTables::Report);
                $link = REMOTE_URL . '?claim=' . $claimToken;
                $link = '<br/>Fai click qui per fare un reclamo: <a href="' . $link . '">' . $link . '</a>';
                switch ($newStatus) {
                    case ReportStatus::InProgress:
                        $mailBody = "Abbiamo preso in carico la tua segnalazione. Riceverai un'altra mail una volta risolto il problema";
                        break;
                    case ReportStatus::Solved:
                        $mailBody = "Abbiamo risolto il problema che ci avevi segnalato! Clicca qui per leggere il messaggio di risposta del nostro addetto." . $link;
                        break;
                    case ReportStatus::Rejected:
                        $mailBody = "La tua richiesta &egrave; stata rifiutata" . $link;
                        break;
                    default:
                        $this->db->rollback();
                        return -5;
                }
                $join = array(
                    'Reports.id' => 'Feedbacks.reportId',
                    'Feedbacks.userId' => 'Users.id'
                );
                $subs = $this->db->listJoin($join, array('Reports.id' => $id));
                if (is_array($subs)) {
                    foreach ($subs as $subUser)
                        mail($subUser['email'], "Aggiornamento sulla segnalazione " . $report['title'], $mailBody);
                }
            }
            return ($q ? 0 : -4);
        }
        return -1;
    }

}