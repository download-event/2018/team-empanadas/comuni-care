function disableAllCategories() {
    for (var i = 1; i < 7; i++)
        $(".img" + i).addClass("sectionInactive");
}
function api(data, callback) {
    $.ajax('api.php',{
        'data': JSON.stringify(data),
        'type': 'POST',
        'processData': false,
        'contentType': 'application/json'
    }).done(callback).fail(function() { callback(false); });
}
function checkDuplicate(obj) {
    var v = obj.value;
    api({
        action: 'checkDuplicate',
        title: v
    }, function (e) {
        if (e !== false) {
            document.getElementById('suggester').innerHTML = e;
        }
    });
}
$(document).ready(function() {
    if (typeof selCat !== 'undefined') {
        for (var i = 1; i < 7; i++)
            $(".img" + i).addClass("sectionDisabled");
        $(".img" + selCat).removeClass("sectionDisabled");
    }
    var $b = $("body");
    $b.on('click', '.reportBox', function() {
        var a = $(this).attr('data-id');
        window.location.href = '?p=report&id=' + a;
    });
    $b.on('click', '.sectionIcon', function() {
        if ($(this).hasClass('iconGeo'))
            window.location.href = '?p=reportslist&view=map';
        else if ($(this).hasClass('iconList'))
            window.location.href = '?p=reportslist&view=list';
        else if ($(this).hasClass('iconProfile'))
            window.location.href = '?p=profile';
        else
            window.location.href = '?p=createreport';
    });
    $b.on('click', '.categories', function() {
        if (typeof selCat === 'undefined') {
            disableAllCategories();
            $(this).removeClass("sectionInactive");
            //alert($(this).attr('data-id'));
            $("#category").val($(this).attr('data-id'));
        }
    });
    $b.on('click', '.submitBtn', function() {
        document.getElementById('mForm').submit();
    });
    $b.on('click', '.bother', function() {
        window.location.href = '?p=report&feedback=1&id=' + reportId;
    });
    $b.on('click', '.danger', function() {
        window.location.href = '?p=report&feedback=2&id=' + reportId;
    });
});